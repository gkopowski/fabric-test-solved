import { fabric } from 'fabric';
import triangleTopRight from '../icons/triangleTopRight.svg';
import triangleBottomLeft from '../icons/triangleBottomLeft.svg';

(function() {
    const degreesToRadians = fabric.util.degreesToRadians;
    const multiplyMatrices = fabric.util.multiplyTransformMatrices;
    const transformPoint = fabric.util.transformPoint;

    fabric.CustomObject = fabric.util.createClass(fabric.Rect, {
        /**
         * Distance between object and its controlling icons (in pixels)
         * @type Number
         * @default
         */
        controlsOffset: 0,

        initialize: function(options) {
            
            options = {
                ...options,
                width: 200,
                height: 200,
                fill: 'yellow',
                padding: 10,
                borderColor: 'black',
                cornerColor: 'black',
                cornerSize: 20,
                hasRotatingPoint: false,
                controlsOffset: 10,
            }

            this.setControlsVisibility({
                mt: false,
                mr: false,
                mb: false,
                ml: false,
            })
            
            this.callSuper('initialize', options);
        }
    });

    fabric.util.object.extend(fabric.CustomObject.prototype, {

        // Overwrite this method to draw custom controls.
        _drawControl: function(control, ctx, methodName, left, top) {
            if (!this.isControlVisible(control)) {
                return;
            }

            const size = this.cornerSize;
            const stroke = !this.transparentCorners && this.cornerStrokeColor;

            let leftPosition = left;
            let topPosition = top;
            let offset;
        
            switch (control) {
                case 'tl':
                case 'br':
                    // 0.5 * ((size * Math.sqrt(2)) - size) part is the distance from
                    // the circle's edge to the corner of the square it is based on.
                    // That value needs to be subtracted from the controlsOffset to have
                    // correct distance from the control to the corner of the object.
                    offset = this.controlsOffset - 0.5 * ((size * Math.sqrt(2)) - size);

                    leftPosition = control === 'tl' ? left - offset : left + offset + size;
                    topPosition = control === 'tl' ? top - offset : top + offset + size;

                    ctx.beginPath();
                    ctx.arc(leftPosition, topPosition, size / 2, 0, 2 * Math.PI, false);
                    ctx.stroke();
                    
                    break;
                case 'bl':
                case 'tr':
                    // Triangle icon is based on the square, so we need to calculate
                    // distance from triangle's base to the corner of that square - 
                    // (size * 0.23).
                    offset = (this.controlsOffset / Math.sqrt(2)) + (size * 0.23);

                    leftPosition = control === 'bl' ? left - offset : left + offset;
                    topPosition = control === 'bl' ? top + offset : top - offset;

                    const icon = new Image();
                    icon.src = control === 'bl' ? triangleBottomLeft : triangleTopRight;

                    ctx.drawImage(icon, leftPosition, topPosition, size, size);

                    break;
                default:
                    this.transparentCorners || ctx.clearRect(left, top, size, size);
                    ctx[methodName + 'Rect'](left, top, size, size);
                    
                    if (stroke) {
                        ctx.strokeRect(left, top, size, size);
                    }
            }
        },

        // Overwrite this method to include controlsOffset in calculations.
        calcCoords: function(absolute) {
            var rotateMatrix = this._calcRotateMatrix(),
                translateMatrix = this._calcTranslateMatrix(),
                startMatrix = multiplyMatrices(translateMatrix, rotateMatrix),
                vpt = this.getViewportTransform(),
                finalMatrix = absolute ? startMatrix : multiplyMatrices(vpt, startMatrix),
                dim = this._getTransformedDimensions(),
                w = dim.x / 2, h = dim.y / 2,
                tl = transformPoint({ x: -w, y: -h }, finalMatrix),
                tr = transformPoint({ x: w, y: -h }, finalMatrix),
                bl = transformPoint({ x: -w, y: h }, finalMatrix),
                br = transformPoint({ x: w, y: h }, finalMatrix);
            if (!absolute) {
                // total padding is sum of the padding and the controlsOffset
              var padding = this.padding + this.controlsOffset, angle = degreesToRadians(this.angle),
                  cos = fabric.util.cos(angle), sin = fabric.util.sin(angle),
                  cosP = cos * padding, sinP = sin * padding, cosPSinP = cosP + sinP,
                  cosPMinusSinP = cosP - sinP;
              if (padding) {
                tl.x -= cosPMinusSinP;
                tl.y -= cosPSinP;
                tr.x += cosPSinP;
                tr.y -= cosPMinusSinP;
                bl.x -= cosPSinP;
                bl.y += cosPMinusSinP;
                br.x += cosPMinusSinP;
                br.y += cosPSinP;
              }
              var ml  = new fabric.Point((tl.x + bl.x) / 2, (tl.y + bl.y) / 2),
                  mt  = new fabric.Point((tr.x + tl.x) / 2, (tr.y + tl.y) / 2),
                  mr  = new fabric.Point((br.x + tr.x) / 2, (br.y + tr.y) / 2),
                  mb  = new fabric.Point((br.x + bl.x) / 2, (br.y + bl.y) / 2),
                  mtr = new fabric.Point(mt.x + sin * this.rotatingPointOffset, mt.y - cos * this.rotatingPointOffset);
            }
      
            // if (!absolute) {
            //   var canvas = this.canvas;
            //   setTimeout(function() {
            //     canvas.contextTop.clearRect(0, 0, 700, 700);
            //     canvas.contextTop.fillStyle = 'green';
            //     canvas.contextTop.fillRect(mb.x, mb.y, 3, 3);
            //     canvas.contextTop.fillRect(bl.x, bl.y, 3, 3);
            //     canvas.contextTop.fillRect(br.x, br.y, 3, 3);
            //     canvas.contextTop.fillRect(tl.x, tl.y, 3, 3);
            //     canvas.contextTop.fillRect(tr.x, tr.y, 3, 3);
            //     canvas.contextTop.fillRect(ml.x, ml.y, 3, 3);
            //     canvas.contextTop.fillRect(mr.x, mr.y, 3, 3);
            //     canvas.contextTop.fillRect(mt.x, mt.y, 3, 3);
            //     canvas.contextTop.fillRect(mtr.x, mtr.y, 3, 3);
            //   }, 50);
            // }
      
            var coords = {
              // corners
              tl: tl, tr: tr, br: br, bl: bl,
            };
            if (!absolute) {
              // middle
              coords.ml = ml;
              coords.mt = mt;
              coords.mr = mr;
              coords.mb = mb;
              // rotating point
              coords.mtr = mtr;
            }
            return coords;
          },
    });
})()

const createFabricObject = () => {
    return new fabric.CustomObject();
}

export default createFabricObject;
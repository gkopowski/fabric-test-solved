import { fabric } from 'fabric';

(function() {
    // This object is needed in the getCornerCursor method.
    const cursorOffset = {
        mt: 0, // n
        tr: 1, // ne
        mr: 2, // e
        br: 3, // se
        mb: 4, // s
        bl: 5, // sw
        ml: 6, // w
        tl: 7 // nw
    };

    fabric.CustomCanvas = fabric.util.createClass(fabric.Canvas, {
        initialize: function(el, options) {
            this.callSuper('initialize', el, options);
        }
    });

    fabric.util.object.extend(fabric.CustomCanvas.prototype, {

        // Overwrite this method to change cursor of tl and br controls.
        getCornerCursor: function(corner, target, e) {
            if (this.actionIsDisabled(corner, target, e)) {
                return this.notAllowedCursor;
            }
            // this needs to be before this._getRotatedCornerCursor method
            // to avoid changing cursor based on the current rotation angle
            // of the object
            else if (corner === 'tl' || corner === 'br') {
                return this.rotationCursor;
            }
            else if (corner in cursorOffset) {
                return this._getRotatedCornerCursor(corner, target, e);
            }
            else if (corner === 'mtr' && target.hasRotatingPoint) {
                return this.rotationCursor;
            }
            else {
                return this.defaultCursor;
            }
        },

        // Overwrite this method to change action of tl and br controls to rotate.
        _getActionFromCorner: function(alreadySelected, corner, e /* target */) {
            if (!corner || !alreadySelected) {
                return 'drag';
            }
      
            switch (corner) {
                case 'tl':
                case 'br':
                    return 'rotate';
                default:
                    return 'scale';
            }
        }
    });
})()

const createFabricCanvas = () => {
    return new fabric.CustomCanvas('canvas');
}

export default createFabricCanvas;